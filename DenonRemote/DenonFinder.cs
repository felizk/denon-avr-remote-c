﻿using System;
using System.Net;
using System.Threading.Tasks;
using ManagedUPnP;

namespace DenonRemote
{
    public class DenonFinder
    {
        public static Task<IPAddress> FindDenonReceiver()
        {
            IPAddress resultIp = null;

            var tcs = new TaskCompletionSource<IPAddress>();
            var d = new Discovery(null);

            d.DeviceAdded += (sender, eventArgs) =>
            {
                if (d == null)
                    return;

                if (eventArgs.Device.ManufacturerName.Equals("DENON", StringComparison.OrdinalIgnoreCase) && eventArgs.Device.RootHostAddresses.Length > 0)
                {
                    resultIp = eventArgs.Device.RootHostAddresses[0];
                    tcs.SetResult(resultIp);

                    d.Dispose();
                    d = null;
                }
            };

            d.SearchComplete += (sender, eventArgs) =>
            {
                if (d == null)
                    return;

                d.Dispose();
                d = null;
            };

            d.Start();

            return tcs.Task;
        }
    }
}
