﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace DenonRemote
{
    public class DenonRemoteConnection : IDisposable
    {
        public static DenonRemoteConnection OpenConnection(IPAddress address)
        {
            var client = new TcpClient();
            client.Connect(address, 23);

            var stream = client.GetStream();

            return new DenonRemoteConnection(stream);
        }

        public void SendCommand(string command)
        {
            var diff = (nextMessage - DateTime.Now).TotalMilliseconds;

            if (diff > 0)
            {
                Thread.Sleep((int)diff);
            }

            writer.WriteLine(command);
            writer.Flush();

            if (command == "PWON" || command == "PWSTANDBY")
            {
                nextMessage = DateTime.Now + TimeSpan.FromSeconds(1);
            }
            else
            {
                nextMessage = DateTime.Now + TimeSpan.FromMilliseconds(20);
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                writer?.Dispose();
                stream?.Dispose();
                stream = null;
            }
        }

        private DenonRemoteConnection(NetworkStream stream)
        {
            this.stream = stream;
            writer = new StreamWriter(stream, Encoding.ASCII) {NewLine = "\r"};
        }

        private NetworkStream stream;
        private DateTime nextMessage = DateTime.Now;

        private readonly StreamWriter writer;
    }
}
