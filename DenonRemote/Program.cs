﻿using System;
using System.Net;

namespace DenonRemote
{
    class Program
    {
        static void Main(string[] args)
        {
            SendDenonCommands(args);
        }

        static bool SendDenonCommands(params string[] commands)
        {
            try
            {
                var ip = FindDenon();

                if (ip == null) return false;

                using (var connection = DenonRemoteConnection.OpenConnection(ip))
                {
                    foreach (var command in commands)
                    {
                        connection.SendCommand(command);
                    }
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        static IPAddress FindDenon()
        {
            var task = DenonFinder.FindDenonReceiver();
            task.Wait();
            return task.Result;
        }
    }
}
