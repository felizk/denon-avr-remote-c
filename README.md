# README #

This is just a quick sample of a C# program that connects to a Denon AVR and uses their protocol to send it messages. I use this to switch to PC input from my controller and back to Xbox One when I'm done using the PC.

I've saved a copy of the protocol in the project folder.